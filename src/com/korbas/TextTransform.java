package com.korbas;

import java.util.*;

public class TextTransform {

    private String text;
    private Set<String> uniqueWords;
    private Set<String> uniqueLetters;
    private Map<String, Set<String>> letterToWords;

    private TextTransform() {
        this.text = "";
        this.uniqueWords = new HashSet<>();
        this.uniqueLetters = new HashSet<>();
        this.letterToWords = new TreeMap<>();
    }

    public TextTransform(String text) {
        this();
        this.text = Objects.requireNonNull(text, "Text must not be null");
    }

    public Map<String, Set<String>> indexLetterWithWordsFromText() {
        setUniqueWordsFromText();
        setUniqueLettersFromText();
        setLetterWordsMap();
        return letterToWords;
    }

    private void setUniqueLettersFromText() {
        char[] chars = text.toLowerCase().toCharArray();
        for (char c : chars) {
            if (Character.isLetter(c)) {
                uniqueLetters.add(Character.toString(c));
            }
        }
    }

    private void setUniqueWordsFromText() {
        String lowerCase = text.replace(",", "").toLowerCase();
        String[] words = lowerCase.split(" ");
        uniqueWords = new HashSet<>(Arrays.asList(words));
    }

    private void setLetterWordsMap() {
        for (String uniqueLetter : uniqueLetters) {
            connectWordsWithLetter(uniqueLetter);
        }
    }

    private void connectWordsWithLetter(String uniqueLetter) {
        for (String uniqueWord : uniqueWords) {
            connectWordWithLetter(uniqueLetter, uniqueWord);
        }
    }

    private void connectWordWithLetter(String letter, String word) {
        if (word.contains(letter)) {
            if (letterToWords.containsKey(letter)) {
                letterToWords.get(letter).add(word);
            } else {
                letterToWords.put(letter, new TreeSet<>(Arrays.asList(word)));
            }
        }
    }
}
