package com.korbas;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        String text = "ala ma kota, kot koduje w Javie Kota";
        Map<String, Set<String>> stringSetMap;
        stringSetMap = new TextTransform(text).indexLetterWithWordsFromText();
        stringSetMap.forEach((key, value) -> System.out.println(key + ": " + String.join(", ", value)));
    }


}
