package com.korbas;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TextTransformTest {

    @Test
    void shouldPassWithOneUniqueLetter() {
        Map<String, Set<String>> result = new TextTransform("Aaa aa a a a a a a aa aaa").indexLetterWithWordsFromText();
        Map<String, Set<String>> expected = new TreeMap<>();

        expected.put("a", new TreeSet<>(Arrays.asList("a", "aa", "aaa")));

        assertEquals(result, expected);
    }

    @Test
    void shouldPassWithOneUniqueWord() {
        Map<String, Set<String>> result = new TextTransform("Ala ala ala").indexLetterWithWordsFromText();
        Map<String, Set<String>> expected = new TreeMap<>();

        expected.put("a", new TreeSet<>(Arrays.asList("ala")));
        expected.put("l", new TreeSet<>(Arrays.asList("ala")));

        assertEquals(result, expected);
    }

    @Test
    void shouldPassForMainText() {
        Map<String, Set<String>> result = new TextTransform("ala ma kota, kot koduje w Javie Kota").indexLetterWithWordsFromText();
        Map<String, Set<String>> expected = new TreeMap<>();
        expected.put("a", new TreeSet<>(Arrays.asList("ala", "javie", "kota", "ma")));
        expected.put("d", new TreeSet<>(Arrays.asList("koduje")));
        expected.put("e", new TreeSet<>(Arrays.asList("javie", "koduje")));
        expected.put("i", new TreeSet<>(Arrays.asList("javie")));
        expected.put("j", new TreeSet<>(Arrays.asList("javie", "koduje")));
        expected.put("k", new TreeSet<>(Arrays.asList("koduje", "kot", "kota")));
        expected.put("l", new TreeSet<>(Arrays.asList("ala")));
        expected.put("m", new TreeSet<>(Arrays.asList("ma")));
        expected.put("o", new TreeSet<>(Arrays.asList("koduje", "kot", "kota")));
        expected.put("t", new TreeSet<>(Arrays.asList("kot", "kota")));
        expected.put("u", new TreeSet<>(Arrays.asList("koduje")));
        expected.put("v", new TreeSet<>(Arrays.asList("javie")));
        expected.put("w", new TreeSet<>(Arrays.asList("w")));
        assertEquals(result, expected);
    }

    @Test
    void shouldReturnEmptyMapWhenStringIsEmpty() {
        Map<String, Set<String>> result = new TextTransform("").indexLetterWithWordsFromText();
        Map<String, Set<String>> expected = new TreeMap<>();
        assertEquals(result, expected);
    }

    @Test()
    void shouldThrowExceptionWhenStringIsNull() {
        assertThrows(NullPointerException.class, () -> {
            new TextTransform(null).indexLetterWithWordsFromText();
        });
    }

    @Test
    void shouldFilterNonLetterCharacters() {
        Map<String, Set<String>> result = new TextTransform("al4 m2 k0ta, kOt k*duje w Jav!e Kota").indexLetterWithWordsFromText();
        Map<String, Set<String>> expected = new TreeMap<>();
        expected.put("a", new TreeSet<>(Arrays.asList("al4", "jav!e", "kota", "k0ta")));
        expected.put("d", new TreeSet<>(Arrays.asList("k*duje")));
        expected.put("e", new TreeSet<>(Arrays.asList("jav!e", "k*duje")));
        expected.put("j", new TreeSet<>(Arrays.asList("jav!e", "k*duje")));
        expected.put("k", new TreeSet<>(Arrays.asList("k*duje", "kot", "k0ta", "kota")));
        expected.put("l", new TreeSet<>(Arrays.asList("al4")));
        expected.put("m", new TreeSet<>(Arrays.asList("m2")));
        expected.put("o", new TreeSet<>(Arrays.asList("kot", "kota")));
        expected.put("t", new TreeSet<>(Arrays.asList("kot", "kota", "k0ta")));
        expected.put("u", new TreeSet<>(Arrays.asList("k*duje")));
        expected.put("v", new TreeSet<>(Arrays.asList("jav!e")));
        expected.put("w", new TreeSet<>(Arrays.asList("w")));
        assertEquals(expected, result);
    }


}
